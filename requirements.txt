dj-database-url==0.5.0
Django==3.1.3
gunicorn==20.0.4
psycopg2==2.8.6
whitenoise==5.2.0
django-import-export==2.4.0
django-dbbackup==3.3.0
