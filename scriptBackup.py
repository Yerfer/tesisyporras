#!usr/bin/env Python
# -*- coding: utf-8 -*-

from datetime import datetime
import subprocess

now = datetime.now()
dt_string = now.strftime("%Y-%m-%d-%H%M%S")
backupurl = subprocess.check_output("heroku pg:backups:url", shell=True).decode()
filepath = "backups/whattopicBackup-{}.dump".format(dt_string)

"""
import requests

print(backupurl)

myfile = requests.get(backupurl, allow_redirects=True)
open(filepath, 'wb').write(myfile.content)

r = requests.get(backupurl)

with open('backups/whattopicBackuptesting.dump', 'wb') as f:
    f.write(r.content)

# Retrieve HTTP meta-data
print(r.status_code)
print(r.headers['content-type'])
print(r.encoding)

import shutil

r = requests.get(backupurl, stream=True)
if r.status_code == 200:
    with open("backups/otro.dump", 'wb') as f:
        r.raw.decode_content = True
        shutil.copyfileobj(r.raw, f)
"""

import urllib.request

opener=urllib.request.build_opener()
opener.addheaders=[('User-Agent','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1941.0 Safari/537.36')]
urllib.request.install_opener(opener)

urllib.request.urlretrieve(backupurl,filepath)