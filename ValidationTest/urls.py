from django.urls import path

from . import views


urlpatterns = [
    #path('', views.indexOld, name='base'),
    #path('', views.indexOld, name='indexOld'),
    path('', views.index, name='index'),
    #path('v3/', views.validation3, name='validation3'),
    #path('v2/', views.validation2, name='validation2'),
    #path('v1/', views.validation1, name='validation1'),
    path('insertV3/', views.insertV3, name='insertV3'),
    path('insertV2/', views.insertV2, name='insertV2'),
    path('insertV1/', views.insertV1, name='insertV1'),
    path('tks/', views.tks, name='tks'),
    path('whattopic/<int:expert_user>/', views.survey, name='survey'),
    path('whattopic/hi=<str:expert_user>/', views.hi, name='hi'),
    path('prueba/', views.formPrueba, name='formPrueba'),
]
