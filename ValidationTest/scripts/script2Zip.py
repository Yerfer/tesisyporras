import os
import zipfile
 
fantasy_zip = zipfile.ZipFile('test.zip', 'w')
rootpath = 'pdf_data'
for folder, subfolders, files in os.walk(rootpath):
 
    for file in files:
        if file.endswith('.pdf'):
            fantasy_zip.write(os.path.join(folder, file), os.path.relpath(os.path.join(folder,file), rootpath), compress_type = zipfile.ZIP_DEFLATED)
 
fantasy_zip.close()