# import module 
import pdf2image
from pdf2image import convert_from_path, convert_from_bytes
  
from pdf2image.exceptions import (
    PDFInfoNotInstalledError,
    PDFPageCountError,
    PDFSyntaxError
)

# Store Pdf with convert_from_path function 
images = convert_from_path(pdf_path='astwood2014.pdf', dpi=100, fmt="jpeg", poppler_path = r"C:\Program Files\poppler-20.12.1\bin") 
  
for i, img in enumerate(images): 
    img.save('test\output2_{}.jpg'.format(i), 'JPEG')
