# Generated by Django 3.1.3 on 2020-12-16 13:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ValidationTest', '0004_auto_20201215_2206'),
    ]

    operations = [
        migrations.AlterField(
            model_name='labelv',
            name='label',
            field=models.CharField(max_length=100),
        ),
    ]
