from django.shortcuts import render
from django.http import HttpResponse#, HttpResponseRedirect
#from django.urls import reverse
#from django.shortcuts import get_object_or_404
import random
from django.shortcuts import redirect
from django.http import JsonResponse
import json
from django.shortcuts import get_object_or_404
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.db.utils import OperationalError
# Import logging from Python's standard library
import logging


#from django.template import loader
#from django.shortcuts import render_to_response
from .models import Term, Expert, Article, Topic, Assignment, LabelV, CoherenceV, AssociationV, ColorTheme, \
    GratitudeQuote, Category, ArticleCategory, ExpertCategory, TopicCategory, AssignmentWeight, RelevantTopic, \
    CoherGrade, ExpertLvl

# Default actived user ID: YERFER
#expert = Expert.objects.get(id=0)

def indexOld(request):
    return HttpResponse("There is nothing here...")

def index(request):
    try:
        return render(request, 'index.html')
    except OperationalError:
        pass

def base(request):
    try:
        expert = Expert.objects.get(id=0)
        #template = loader.get_template('whattopic/index.html')
        #expert = Expert.objects.get(id=1)
        context = {
            'expert': expert
        }
    except OperationalError:
        pass  # happens when db doesn't exist yet, views.py should be
        # importable without this side effect
    return render(request, 'base.html', context)


def validation2(request):
    try:
        expert = Expert.objects.get(id=0)
        terms_list = Term.objects.order_by('?')[:15]
        article = Article.objects.get(id=124)
        #expert = Expert.objects.get(id=0)
        context = {
            'terms_list': terms_list,
            'expert': expert,
            'article': article
        }
    except OperationalError:
        pass
    return render(request, 'validation2.html', context)

def insertV2(request):
    try:
        if request.method == 'POST':
            id_term_data = request.POST.get('id_term_list2string').split(",")
            id_article = request.POST.get('id_article')
            id_expert = request.POST.get('id_expert')
            id_coherGrade = request.POST.get('id_cohereGrade2')
            id_Unchecked_term_list2string = request.POST.get('id_term_unselected')
            print("id_Unchecked_term_list2string: {}".format(id_Unchecked_term_list2string))
            print("id_term_data: {}".format(id_term_data))
            restTerms = "[{}]".format(id_Unchecked_term_list2string)
            #dataJson = [{'id_term_0':id_term_0, 'id_term_1':id_term_1, 'id_term_2':id_term_2, 'id_term_3':id_term_3, 'id_term_4':id_term_4, 'id_article':id_article, 'id_expert':id_expert}]

            id_article_model = Article.objects.filter(id=id_article)[0]
            id_expert_model = Expert.objects.filter(id=id_expert)[0]
            id_coherGrade_model = CoherGrade.objects.filter(id=id_coherGrade)[0]

            for i, id_term_i in enumerate(id_term_data):
                id_term_i_model = Term.objects.filter(id=id_term_i)[0]
                insert_coherenceV = CoherenceV(id_article=id_article_model, id_term=id_term_i_model,
                                               id_expert=id_expert_model, order=i, id_cohergrade=id_coherGrade_model,
                                               restTerms=restTerms)
                insert_coherenceV.save()

            returnJson = [{'status':200}]
            return HttpResponse(json.dumps(returnJson), content_type='application/json')
        else:
            return redirect("base")
    except OperationalError:
        pass

def insertV2OldFail(request):
    try:
        print("Entramos en insertV2")
        if request.is_ajax and request.method == "POST":
            print(request.POST)
            print(request.POST['id_term_3'])
            return JsonResponse({"valid": True}, status = 200)
        return JsonResponse({"error": ""}, status=400)
    except OperationalError:
        pass

def validation1(request):
    try:
        expert = Expert.objects.get(id=0)
        terms_list = Term.objects.order_by('?')[:15]
        article = Article.objects.get(id=124)
        #expert = Expert.objects.get(id=0)
        context = {
            'terms_list': terms_list,
            'expert': expert,
            'article': article
        }
        return render(request, 'validation1.html', context)
    except OperationalError:
        pass

def insertV1(request):
    try:
        if request.method == 'POST':
            id_term_data = request.POST.get('id_term_list2string').split(",")
            id_Unchecked_term_list2string = request.POST.get('id_Unchecked_term_list2string')
            print("id_Unchecked_term_list2string: {}".format(id_Unchecked_term_list2string))
            print("id_term_data: {}".format(id_term_data))
            id_article = request.POST.get('id_article')
            id_expert = request.POST.get('id_expert')
            id_coherGrade = request.POST.get('id_cohereGrade1')
            restTerms = "[{}]".format(id_Unchecked_term_list2string)
            #dataJson = [{'id_term_0':id_term_0, 'id_term_1':id_term_1, 'id_term_2':id_term_2, 'id_term_3':id_term_3, 'id_term_4':id_term_4, 'id_article':id_article, 'id_expert':id_expert}]
            #print(data)

            id_article_model = Article.objects.filter(id=id_article)[0]
            id_expert_model = Expert.objects.filter(id=id_expert)[0]
            id_coherGrade_model = CoherGrade.objects.filter(id=id_coherGrade)[0]

            #This is to zero selected terms, by deafult <<NONE>> is saved.
            if len(id_term_data)<1:
                id_term_data=0

            for id_term_i in id_term_data:
                id_term_i_model = Term.objects.filter(id=id_term_i)[0]
                insert_associationV = AssociationV(id_article=id_article_model, id_term=id_term_i_model,
                                                   id_expert=id_expert_model, id_cohergrade=id_coherGrade_model,
                                                   restTerms=restTerms)
                insert_associationV.save()

            returnJson = [{'status':200}]
            return HttpResponse(json.dumps(returnJson), content_type='application/json')
        else:
            return redirect("base")
    except OperationalError:
        pass

def validation3(request):
    try:
        expert = Expert.objects.get(id=0)
        id_topic = Topic.objects.get(id=random.randint(0, 2))
        term_assign_list = Assignment.objects.filter(id_topic__id=id_topic.id)  # .only('id').all()
        terms_list = term_assign_list
        # terms_list = Term.objects.filter(id__id_term=term_assign_list).order_by('?')[:10]
        context = {
            'terms_list': terms_list,
            'expert': expert,
            'id_topic': id_topic,
            'term_assign_list': term_assign_list
        }
        return render(request, 'validation3.html', context)
    except OperationalError:
        pass

def insertV3(request):
    try:
        if request.method == 'POST':
            topicLabel = request.POST.get('topicLabel').split(",")
            print("topiclabel: {}".format(topicLabel))
            #topicDescription = request.POST.get('topicDescription')
            id_topic = request.POST.get('id_topic')
            id_expert = request.POST.get('id_expert')
            id_expertLvl = request.POST.get('id_expertLvl')

            id_topicM = Topic.objects.filter(id=id_topic)[0]
            id_expertM = Expert.objects.filter(id=id_expert)[0]
            id_expertLvl_model = ExpertLvl.objects.filter(id=id_expertLvl)[0]

            for topicLabel_x in topicLabel:
                insert_labelV = LabelV(id_topic=id_topicM, id_expert=id_expertM, label=topicLabel_x, id_expertlvl=id_expertLvl_model)
                insert_labelV.save()

            returnJson = [{'status':200}]
            return HttpResponse(json.dumps(returnJson), content_type='application/json')
        else:
            return redirect('base')
    except OperationalError:
        pass

def tks(request):
    return HttpResponse("Gracias por su tiempo!")

def survey1(request):
    try:
        expert = Expert.objects.get(id=0)
        terms_list = Term.objects.order_by('?')[:15]
        article = Article.objects.get(id=124)
        id_topic = Topic.objects.get(id=random.randint(0, 2))
        term_assign_list = Assignment.objects.filter(id_topic__id=id_topic.id)

        print("printing data:")
        print("terms_list:{}".format(terms_list))
        print("article:{}".format(article))
        print("id_topic:{}".format(id_topic))
        print("term_assign_list:{}".format(term_assign_list))

        context = {
            # To everyone
            'expert': expert,

            # To V1 and V2
            'terms_list': terms_list,
            'article': article,

            # To V3
            'id_topic': id_topic,
            'term_assign_list': term_assign_list
        }

        return render(request, 'survey.html', context)
    except OperationalError:
        pass

def existsUnlabeledTopic(topicCategory_list, expert):
    try:
        for topicCategory_x in topicCategory_list:
            id_topic_x = topicCategory_x.id_topic.id
            try:
                obj = LabelV.objects.get(id_topic=id_topic_x, id_expert=expert.id)
            except MultipleObjectsReturned:
                pass
            except ObjectDoesNotExist:
                #print("Label not found")
                #print("id_topic_x: "+str(id_topic_x))
                #print("expert.id: "+str(expert.id))
                term_assign_listX_V3 = Assignment.objects.filter(id_topic__id=id_topic_x)
                #print("term_assign_listX_V3: "+str(term_assign_listX_V3.__len__()))
                topic_x = Topic.objects.get(id=id_topic_x)
                return True, topic_x, term_assign_listX_V3
        return False, None, None
    except OperationalError:
        pass

#Here was selected top-5 terms of top-5 topics = 25 terms.
def existsUnsolvedArticle(articleCategory_list, expert):
    try:
        for articleCategory_x in articleCategory_list:
            id_article_x = articleCategory_x.id_article.id
            try:
                obj = AssociationV.objects.get(id_article=id_article_x, id_expert=expert.id)
            except MultipleObjectsReturned:
                pass
            except ObjectDoesNotExist:
                relevantTopics = RelevantTopic.objects.filter(id_article_id=id_article_x).order_by("-topic_coherence")[:5]
                relevantTopicsList = relevantTopics.values("id_topic")
                term_assign_listX_v1 = Assignment.objects.filter(id_topic__id=relevantTopicsList[0]['id_topic'])[:5] #Top-5 relevant topics
                temporalTermDict = term_assign_listX_v1.values('id_term__id')
                temporalTermList = []
                for termId in temporalTermDict:
                    temporalTermList.append(termId['id_term__id'])
                for relevantTopicX in relevantTopicsList[1:4]:
                    term_assign_listX_v1_temp = Assignment.objects.filter(id_topic__id=relevantTopicX['id_topic'])
                    term_assign_listX_v1_temp = term_assign_listX_v1_temp.exclude(id_term__in = temporalTermList)
                    term_assign_listX_v1_temp = term_assign_listX_v1_temp[:5]
                    for termId in term_assign_listX_v1_temp:
                        temporalTermList.append(termId.id_term)
                    term_assign_listX_v1 = term_assign_listX_v1 | term_assign_listX_v1_temp
                term_assign_listX_v1 = term_assign_listX_v1.all().order_by("id_term__term")
                article_x = Article.objects.get(id=id_article_x)
                return True, article_x, term_assign_listX_v1
        return False, None, None
    except OperationalError:
        pass

# Here we select only 25 weighted terms from all topics
def existsUnsolvedArticleWeight(articleCategory_list, expert):
    try:
        temporalTermList2 = []
        for articleCategory_x in articleCategory_list:
            id_article_x = articleCategory_x.id_article.id
            try:
                obj = CoherenceV.objects.get(id_article=id_article_x, id_expert=expert.id)
            except MultipleObjectsReturned:
                pass
            except ObjectDoesNotExist:
                term_assign_listX_v2 = AssignmentWeight.objects.filter(id_article_id=id_article_x).order_by("-coheren_weight")
                temporalTermDict = term_assign_listX_v2.values('id_term__id').distinct()
                term_assign_listX_v2_final = term_assign_listX_v2.filter(id_term_id=temporalTermDict[0]['id_term__id'])
                temporalTermList2.append(temporalTermDict[0]['id_term__id'])
                counter = 1
                for termId in temporalTermDict[1:]:
                    if counter==25:
                        break
                    if (termId['id_term__id'] in temporalTermList2):
                        pass
                    else:
                        temporalTermList2.append(termId['id_term__id'])
                        term_assign_listX_v2_temp = term_assign_listX_v2.filter(id_term_id=termId['id_term__id'])
                        if term_assign_listX_v2_temp.__len__() > 1:
                            first_IdAssignmentWeight_dup_term_ = term_assign_listX_v2_temp.order_by(
                                'coheren_weight').first().pk
                            term_assign_listX_v2_temp = AssignmentWeight.objects.filter(
                                pk=first_IdAssignmentWeight_dup_term_)
                        term_assign_listX_v2_final = term_assign_listX_v2_final | term_assign_listX_v2_temp
                        counter+=1

                ## This is old app. top-5 weighted terms into top-5 topics.
                """
                term_assign_listX_v2 = AssignmentWeight.objects.filter(id_article_id=id_article_x)
                print("term_assign_listX_v2 {}".format(term_assign_listX_v2))
                print("id_article_x: {}".format(id_article_x))
                temporalTermDict = term_assign_listX_v2.values('id_term__id')
                term_assign_listX_v2_final = term_assign_listX_v2.filter(id_term_id=temporalTermDict[0]['id_term__id'])
                if term_assign_listX_v2_final.__len__() > 1:
                    first_IdAssignmentWeight_dup_term_ = term_assign_listX_v2_final.order_by('coheren_weight').first().pk
                    term_assign_listX_v2_final = AssignmentWeight.objects.filter(pk=first_IdAssignmentWeight_dup_term_)
                temporalTermList2.append(temporalTermDict[0]['id_term__id'])
                counter = 1
                counterNeg = 0
                for termId in temporalTermDict[1:]:
                    if (counter in [0,1,2,3,4]):
                        print("Entro termId['id_term__id'] {}".format(termId['id_term__id']))
                        if (termId['id_term__id'] in temporalTermList2):
                            print("id_term existente: {}".format(termId['id_term__id']))
                            counterNeg += 1
                        else:
                            temporalTermList2.append(termId['id_term__id'])
                            term_assign_listX_v2_temp = term_assign_listX_v2.filter(id_term_id=termId['id_term__id'])
                            if term_assign_listX_v2_temp.__len__() > 1:
                                first_IdAssignmentWeight_dup_term_ = term_assign_listX_v2_temp.order_by('coheren_weight').first().pk
                                term_assign_listX_v2_temp = AssignmentWeight.objects.filter(pk=first_IdAssignmentWeight_dup_term_)
                            term_assign_listX_v2_final = term_assign_listX_v2_final | term_assign_listX_v2_temp
                            counter+=1
                        print("temporalTermList2 {}".format(temporalTermList2))
                    else:
                        print("ELSE: termID {}".format(termId))
                        counterNeg+=1
                        if (counterNeg>4):
                            print("Reinicio...................")
                            counter=0
                            counterNeg = 0
                """

                term_assign_listX_v2_final = term_assign_listX_v2_final.order_by("?")
                article_x = Article.objects.get(id=id_article_x)
                return True, article_x, term_assign_listX_v2_final
        return False, None, None
    except OperationalError:
        pass

def survey(request, expert_user):
    try:
        # This was to search by id got from URL, local version
        # expert = get_object_or_404(Expert, pk=expert_id)
        # This is current version in production
        expert = get_object_or_404(Expert, user=expert_user)

        terms_list = Term.objects.order_by('?')[:15]
        article = Article.objects.get(id=124)

        id_topic = Topic.objects.get(id=random.randint(0, 2))
        term_assign_list = Assignment.objects.filter(id_topic__id=id_topic.id)

        expertCategories_list = ExpertCategory.objects.filter(id_expert__id=expert.id)

        print("*********************************************")
        print("EXPERT --{}-- validating... <{}>".format(expert.name, expertCategories_list.get(id_expert__id=expert.id)))
        print("*********************************************")

        # Categories for x expert
        categoriesNumber = expertCategories_list.__len__()

        ###### Get all articles by each one category
        existsBooleanV1 = False
        article_x_v1 = None
        term_assign_listX_v1 = None
        # Now search in each one categories some article
        for articleCategory_x in expertCategories_list:
            articleCategory_list = ArticleCategory.objects.filter(id_category__id=articleCategory_x.id_category.id).order_by("?")
            existsBooleanV1, article_x_v1, term_assign_listX_v1 = existsUnsolvedArticle(articleCategory_list, expert)
            if (existsBooleanV1):
                break
        cohereGrade1 = CoherGrade.objects.all()
        if (not existsBooleanV1):
            print("----->>>> V1: No articles to evaluate...")

        existsBooleanV2 = False
        article_x_v2 = None
        term_assign_listX_v2 = None
        # Now search in each one categories some article
        for articleCategory_x in expertCategories_list:
            #Here it's a edit because only we use the same articleCategory_list that V1 to force the same articles into V1 and V2.
            #articleCategory_list = ArticleCategory.objects.filter(id_category__id=articleCategory_x.id_category.id)
            existsBooleanV2, article_x_v2, term_assign_listX_v2 = existsUnsolvedArticleWeight(articleCategory_list, expert)
            if (existsBooleanV2):
                break
        cohereGrade2 = CoherGrade.objects.all()
        if (not existsBooleanV2):
            print("----->>>> V2: No articles to evaluate...")

        ######## Get all topics by each one category
        ### Here get topicCategory list only in one category in position 0
        #topicCategory_list = TopicCategory.objects.filter(id_category__id=expertCategories_list[0].id_category.id)

        ######## Here looks for some topic in filtered category
        #rtaBoolean, id_topic_x, term_assign_listX = existsUnlabeledTopic(topicCategory_list, expert)

        ######## Join the both above sentences
        existsBooleanV3 = False
        id_topic_x = None
        term_assign_listX = None
        # Now search in each one categories some topic
        for topicCategory_x in expertCategories_list:
            #print("searching topicCategory_list")
            #print("topicCategory_x.id_category.id: {}".format(topicCategory_x.id_category.id))
            topicCategory_list = TopicCategory.objects.filter(id_category__id=topicCategory_x.id_category.id)\
                .order_by("-id_topic_id__coherence_gral")
            #print("topicCategory_list: {}".format(topicCategory_list))
            # Returns False when find the first existence of some UnlabeledTopic
            # returns True while not.
            existsBooleanV3, topic_x, term_assign_listX = existsUnlabeledTopic(topicCategory_list, expert)
            #print("returning values of id_topic_x and term_assign_listX")
            if (existsBooleanV3):
                break
        if(not existsBooleanV3):
            print("----->>>> V3: No assigned categories to any topic, sending Default Category...")
            # Join with Default topicCategory: id=0
            defaultTopicCategory = TopicCategory.objects.filter(id_category__id=0)\
                .order_by("-id_topic_id__coherence_gral")
            existsBooleanV3, topic_x, term_assign_listX = existsUnlabeledTopic(defaultTopicCategory, expert)
            # print("returning values of id_topic_x and term_assign_listX")

        expertLvl = ExpertLvl.objects.all()

        # Get one random gratitude quote to "tks" section.
        quote = GratitudeQuote.objects.order_by('?')[0]

        randomExpertCategory = expertCategories_list.order_by('?')[0]
        randomfilteredCategory = Category.objects.get(id=randomExpertCategory.id_category_id)

        ### Data report
        """
        print("********************************")
        print("New expert evaluating:")
        print("********************************")
        print("expert: {}\n--------------------".format(expert))
        print("categoriesNumber: {}\n--------------------".format(categoriesNumber))
        print("expertCategories_list: {}\n--------------------".format(expertCategories_list))
        print("example: {}\n--------------------".format(expertCategories_list[0].id_category.id))
        print("article_x_v1:{}\n--------------------".format(article_x_v1))
        #print("term_assign_listX_v1:{}\n--------------------".format(term_assign_listX_v1))
        print("article_x_v2:{}\n--------------------".format(article_x_v2))
        print("term_assign_listX_v2:{}\n--------------------".format(term_assign_listX_v2))
        print("topic_x:{}\n--------------------".format(topic_x))
        print("term_assign_listX:{}\n--------------------".format(term_assign_listX))
        print("Category colortheme: {}".format(randomfilteredCategory.id_colortheme.name))
        print("********************************")
        """

        ### Test data
        articlePDF1 = 'porras-garcia2018.pdf'
        articlePDF2 = 'porras-garcia2018.pdf'

        context = {
            # To everyone
            'expert': expert,

            # To V1
            'term_assign_listX_v1': term_assign_listX_v1,
            'article_x_v1': article_x_v1,
            'cohereGrade1': cohereGrade1,

            # To V2
            'term_assign_listX_v2': term_assign_listX_v2,
            'article_x_v2': article_x_v2,
            'cohereGrade2': cohereGrade2,

            # To V3
            'id_topic': topic_x,
            'term_assign_list': term_assign_listX,
            'expertLvl': expertLvl,

            # To Tks
            'quote': quote,

            # To colorTheme
            'randomfilteredCategory': randomfilteredCategory
        }
        return render(request, 'survey.html', context)
    except OperationalError:
        pass

def formPrueba(request):
    try:
        if request.method == 'POST':
            print("INGRESO VISTA ")
            #return HttpResponse("Ingresó por POST")
            return tks(request)
            #return redirect('index')
        else:
            return render(request, 'formPrueba.html')
    except OperationalError:
        pass

def hi(request, expert_user):
    try:
        expert = {'name': expert_user}
        context = {
            'expert': expert
        }
        return render(request, 'base.html', context)
    except OperationalError:
        pass

# Create a logger for this file
logger = logging.getLogger(__file__)

def some_view(request):
    """
    Example view showing all the ways you can log messages.
    """
    logger.debug("This logs a debug message.")
    logger.info("This logs an info message.")
    logger.warning("This logs a warning message.")
    logger.error("This logs an error message.")
    try:
        raise Exception("This is a handled exception")
    except Exception:
        logger.exception("This logs an exception.")

    raise Exception("This is an unhandled exception")
    return HttpResponse("this worked")