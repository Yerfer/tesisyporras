from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import Article, Expert, Topic, Term, Assignment, AssociationV, CoherenceV, LabelV, ColorTheme, \
    Category, ArticleCategory, ExpertCategory, TopicCategory, GratitudeQuote, AssignmentWeight, RelevantTopic, \
    CoherGrade, ExpertLvl


#ImportExportModelAdmin.to_encoding="utf-8-sig"
#ImportExportModelAdmin.from_encoding="utf-8-sig"

# version 1

class ArticleResource(resources.ModelResource):
    class Meta:
        model = Article
class ArticleAdmin(ImportExportModelAdmin):
    resource_class = ArticleResource

class ExpertResource(resources.ModelResource):
    class Meta:
        model = Expert
class ExpertAdmin(ImportExportModelAdmin):
    resource_class = ExpertResource

class TopicResource(resources.ModelResource):
    class Meta:
        model = Topic
class TopicAdmin(ImportExportModelAdmin):
    resource_class = TopicResource

class TermResource(resources.ModelResource):
    class Meta:
        model = Term
class TermAdmin(ImportExportModelAdmin):
    resource_class = TermResource

class AssignmentResource(resources.ModelResource):
    class Meta:
        model = Assignment
class AssignmentAdmin(ImportExportModelAdmin):
    resource_class = AssignmentResource

class AssociationVResource(resources.ModelResource):
    class Meta:
        model = AssociationV
class AssociationVAdmin(ImportExportModelAdmin):
    resource_class = AssociationVResource

class CoherenceVResource(resources.ModelResource):
    class Meta:
        model = CoherenceV
class CoherenceVAdmin(ImportExportModelAdmin):
    resource_class = CoherenceVResource

class LabelVResource(resources.ModelResource):
    class Meta:
        model = LabelV
class LabelVAdmin(ImportExportModelAdmin):
    resource_class = LabelVResource


########## version 2: adding  ##################

class GratitudeQuoteResource(resources.ModelResource):
    class Meta:
        model = GratitudeQuote
class GratitudeQuoteAdmin(ImportExportModelAdmin):
    resource_class = GratitudeQuoteResource


class ColorThemeResource(resources.ModelResource):
    class Meta:
        model = ColorTheme
class ColorThemeAdmin(ImportExportModelAdmin):
    resource_class = ColorThemeResource


class CategoryResource(resources.ModelResource):
    class Meta:
        model = Category
class CategoryAdmin(ImportExportModelAdmin):
    resource_class = CategoryResource


class ArticleCategoryResource(resources.ModelResource):
    class Meta:
        model = ArticleCategory
class ArticleCategoryAdmin(ImportExportModelAdmin):
    resource_class = ArticleCategoryResource


class ExpertCategoryResource(resources.ModelResource):
    class Meta:
        model = ExpertCategory
class ExpertCategoryAdmin(ImportExportModelAdmin):
    resource_class = ExpertCategoryResource


class TopicCategoryResource(resources.ModelResource):
    class Meta:
        model = TopicCategory
class TopicCategoryAdmin(ImportExportModelAdmin):
    resource_class = TopicCategoryResource


class AssignmentWeightResource(resources.ModelResource):
    class Meta:
        model = AssignmentWeight
class AssignmentWeightAdmin(ImportExportModelAdmin):
    resource_class = AssignmentWeightResource


########### Version 3 ##########
class RelevantTopicResource(resources.ModelResource):
    class Meta:
        model = RelevantTopic
class RelevantTopicAdmin(ImportExportModelAdmin):
    resource_class = RelevantTopicResource


########### Version 4 ##########
class CoherGradeResource(resources.ModelResource):
    class Meta:
        model = CoherGrade
class CoherGradeAdmin(ImportExportModelAdmin):
    resource_class = CoherGradeResource


########### Version 5 ##########
class ExpertLvlResource(resources.ModelResource):
    class Meta:
        model = ExpertLvl
class ExpertLvlAdmin(ImportExportModelAdmin):
    resource_class = ExpertLvlResource


# version 1
admin.site.register(Article, ArticleAdmin)
admin.site.register(Expert, ExpertAdmin)
admin.site.register(Topic, TopicAdmin)
admin.site.register(Term, TermAdmin)
admin.site.register(Assignment, AssignmentAdmin)
admin.site.register(AssociationV, AssociationVAdmin)
admin.site.register(CoherenceV, CoherenceVAdmin)
admin.site.register(LabelV, LabelVAdmin)

# version 2: adding
admin.site.register(GratitudeQuote, GratitudeQuoteAdmin)
admin.site.register(ColorTheme, ColorThemeAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(ArticleCategory, ArticleCategoryAdmin)
admin.site.register(ExpertCategory, ExpertCategoryAdmin)
admin.site.register(TopicCategory, TopicCategoryAdmin)
admin.site.register(AssignmentWeight, AssignmentWeightAdmin)

# version 3
admin.site.register(RelevantTopic, RelevantTopicAdmin)

# version 4
admin.site.register(CoherGrade, CoherGradeAdmin)

# version 5
admin.site.register(ExpertLvl, ExpertLvlAdmin)

#admin.site.register(Article)
#admin.site.register(Expert)
#admin.site.register(Topic)
#admin.site.register(Term)
#admin.site.register(Assignment)
#admin.site.register(AssociationV)
#admin.site.register(CoherenceV)
#admin.site.register(LabelV)