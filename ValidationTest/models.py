# -*- coding: utf-8 -*-

from django.db import models


class Article(models.Model):
    class Meta:
        verbose_name = 'Article'
        verbose_name_plural = 'Articles'
    id = models.IntegerField(primary_key=True)
    title = models.TextField(max_length=400)
    abstract = models.TextField(max_length=4000)
    pdfPath = models.CharField(max_length=100, default="-")
    #cosaArtAA = models.CharField(max_length=100, default="-")

    def __str__(self):
        return str(self.id)+" - "+self.title


class Expert(models.Model):
    class Meta:
        verbose_name = 'Expert'
        verbose_name_plural = 'Experts'
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200)
    user = models.CharField(max_length=2000)
    email = models.EmailField(max_length=50)
    #cosaExpAA = models.CharField(max_length=100, default="-")

    def __str__(self):
        return self.name


class Topic(models.Model):
    class Meta:
        verbose_name = 'Topic'
        verbose_name_plural = 'Topics'
    id = models.IntegerField(primary_key=True)
    coherence_gral = models.DecimalField(default=0.0, max_digits=14, decimal_places=12)

    def __str__(self):
        return str(self.id) + " - " + str(self.coherence_gral)


class Term(models.Model):
    class Meta:
        verbose_name = 'Term'
        verbose_name_plural = 'Terms'
    id = models.IntegerField(primary_key=True)
    term = models.CharField(max_length=60)
    term_es = models.CharField(max_length=60, default="-")

    def __str__(self):
        return (str(self.id) + " - " + self.term + " - " + self.term_es)


class GratitudeQuote(models.Model):
    class Meta:
        verbose_name = 'GratitudeQuote'
        verbose_name_plural = 'GratitudeQuotes'
    quote = models.TextField(max_length=400, default="-")
    quote_es = models.TextField(max_length=400, default="-")
    author = models.CharField(max_length=40)

    def __str__(self):
        return self.author


class ColorTheme(models.Model):
    class Meta:
        verbose_name = 'ColorTheme'
        verbose_name_plural = 'ColorThemes'

    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    logoC = models.CharField(max_length=10, default="")
    bodyC = models.CharField(max_length=10)
    articleTitleC = models.CharField(max_length=10)
    abstractTitleC = models.CharField(max_length=10)
    borderTermListC = models.CharField(max_length=10)
    topicTermsC = models.CharField(max_length=10)

    def __str__(self):
        return self.name


class Category(models.Model):
    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    id_colortheme = models.ForeignKey(ColorTheme, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class CoherGrade(models.Model):
    class Meta:
        verbose_name = 'CoherGrade'
        verbose_name_plural = 'CoherGrades'
    grade = models.CharField(max_length=25)

    def __str__(self):
        return self.grade


class ExpertLvl(models.Model):
    class Meta:
        verbose_name = 'ExpertLvl'
        verbose_name_plural = 'ExpertLvls'
    lvl = models.CharField(max_length=25)

    def __str__(self):
        return self.lvl


class Assignment(models.Model):
    class Meta:
        verbose_name = 'Assignment'
        verbose_name_plural = 'Assignments'

    id_term = models.ForeignKey(Term, null=True, blank=True, on_delete=models.CASCADE)
    id_topic = models.ForeignKey(Topic, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id_term)


class AssignmentWeight(models.Model):
    class Meta:
        verbose_name = 'AssignmentWeight'
        verbose_name_plural = 'AssignmentWeights'

    coheren_weight = models.DecimalField(default=0.0, max_digits=14, decimal_places=12)
    id_article = models.ForeignKey(Article, null=True, blank=True, on_delete=models.CASCADE)
    id_term = models.ForeignKey(Term, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id_term)


class RelevantTopic(models.Model):
    class Meta:
        verbose_name = 'RelevantTopic'
        verbose_name_plural = 'RelevantTopics'

    topic_coherence = models.DecimalField(default=0.0, max_digits=14, decimal_places=12)
    id_article = models.ForeignKey(Article, null=True, blank=True, on_delete=models.CASCADE)
    id_topic = models.ForeignKey(Topic, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.pk)

class ArticleCategory(models.Model):
    class Meta:
        verbose_name = 'ArticleCategory'
        verbose_name_plural = 'ArticleCategories'
    id_article = models.ForeignKey(Article, null=True, blank=True, on_delete=models.CASCADE)
    id_category = models.ForeignKey(Category, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id) + " | " + str(self.id_article) + " [ " + str(self.id_category) + " ]"


class ExpertCategory(models.Model):
    class Meta:
        verbose_name = 'ExpertCategory'
        verbose_name_plural = 'ExpertCategories'
    id_expert = models.ForeignKey(Expert, null=True, blank=True, on_delete=models.CASCADE)
    id_category = models.ForeignKey(Category, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id_expert) + " - " + str(self.id_category)


class TopicCategory(models.Model):
    class Meta:
        verbose_name = 'TopicCategory'
        verbose_name_plural = 'TopicCategories'
    id_topic = models.ForeignKey(Topic, null=True, blank=True, on_delete=models.CASCADE)
    id_category = models.ForeignKey(Category, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.pk)


class AssociationV(models.Model):
    class Meta:
        verbose_name = 'AssociationV'
        verbose_name_plural = 'AssociationVs'
    restTerms = models.CharField(max_length=200, default="[]")
    id_article = models.ForeignKey(Article, null=True, blank=True, on_delete=models.CASCADE)
    id_term = models.ForeignKey(Term, null=True, blank=True, on_delete=models.CASCADE)
    id_expert = models.ForeignKey(Expert, null=True, blank=True, on_delete=models.CASCADE)
    id_cohergrade = models.ForeignKey(CoherGrade, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.pk) + " | Art: " + str(self.id_article) + " | Term: " + str(self.id_term) + " | Exp: " + str(self.id_expert)


class CoherenceV(models.Model):
    class Meta:
        verbose_name = 'CoherenceV'
        verbose_name_plural = 'CoherenceVs'
    order = models.IntegerField(default=0)
    restTerms = models.CharField(max_length=200, default="[]")
    id_article = models.ForeignKey(Article, null=True, blank=True, on_delete=models.CASCADE)
    id_term = models.ForeignKey(Term, null=True, blank=True, on_delete=models.CASCADE)
    id_expert = models.ForeignKey(Expert, null=True, blank=True, on_delete=models.CASCADE)
    id_cohergrade = models.ForeignKey(CoherGrade, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.pk) + " | Art: " + str(self.id_article) + " | Term: " + str(self.id_term) + " | Exp: " + str(self.id_expert)


class LabelV(models.Model):
    class Meta:
        verbose_name = 'LabelV'
        verbose_name_plural = 'LabelVs'
    label = models.CharField(max_length=100)
    #description = models.CharField(max_length=100)
    id_topic = models.ForeignKey(Topic, null=True, blank=True, on_delete=models.CASCADE)
    id_expert = models.ForeignKey(Expert, null=True, blank=True, on_delete=models.CASCADE)
    id_expertlvl = models.ForeignKey(ExpertLvl, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.pk) + " | " +self.label + " - Topic: " + str(self.id_topic) + " - " + str(self.id_expert) + " - " + str(self.id_expertlvl)