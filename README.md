# WhatTopic #

Instrument developed by Yerson Porras.\
[Personal site](https://www.yerfer.site/)

### What is this repository for? ###

* Instrument for the stage of qualitative evaluation of the results obtained by topic modeling.
* Version 1.4X
* [App Link](https://www.yerfer.site/whattopic/hi=Usted)