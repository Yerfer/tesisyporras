import sqlite3
import pandas as pd
from sqlite3 import Error

prefixDjango = "ValidationTest_"

def sql_connection():

    try:

        con = sqlite3.connect('db - copia.sqlite3')
        print("Connection is established: Database is loaded!")
        return con
    except Error:
        print(Error)
    #finally:
    #    con.close()

def sql_select(con):
    cursorObj = con.cursor()

def sql_fetch(con):
    cursorObj = con.cursor()
    cursorObj.execute('SELECT * FROM {}Category'.format(prefixDjango))
    rows = cursorObj.fetchall()
    for row in rows:
        print(row)

def db2PD(con):
	# Usa read_sql_query de pandas para extraer el resultado
	# de la consulta a un DataFrame
	df = pd.read_sql_query('SELECT * FROM {}Category'.format(prefixDjango), con)
	# Verifica que el resultado de la consulta SQL está
	# almacenado en el DataFrame
	print(df.head())

con = sql_connection()

sql_fetch(con)

db2PD(con)

con.close()